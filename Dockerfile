FROM ubuntu:bionic

RUN apt update                                && \
    apt install -y                               \
        zlib1g-dev                               \
        libssl1.0-dev                            \
        nodejs                                   \
        node-gyp                                 \
        nodejs-dev                               \
        npm                                   && \
    npm install -g simple-breakpad-server     && \
    rm -rf /var/lib/apt/lists/*


EXPOSE 1127

RUN mkdir -p /breakpad/data && mkdir -p /breakpad/config
VOLUME [ "/breakpad" ]
WORKDIR /breakpad/config

ENTRYPOINT ["simple-breakpad-server"]